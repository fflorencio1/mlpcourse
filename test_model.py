import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical

# Load the dataset
_, (x_test, y_test) = mnist.load_data()

# Normalize pixel values to the range [0, 1]
x_test = x_test / 255.0

# Transform labels into one-hot encoding
y_test_onehot = to_categorical(y_test, num_classes=10)

# Load the model
model = tf.keras.models.load_model("trained_model.h5")

# Calculate loss and accuracy
loss, accuracy = model.evaluate(x_test, y_test_onehot)
print(f"Teste - Perda: {loss}, Acurácia: {accuracy}")
from flask import Flask, request, jsonify
import tensorflow as tf
from tensorflow.keras.models import load_model
import numpy as np

app = Flask(__name__)

# Load the model
model = load_model("trained_model.h5")

@app.route("/predict", methods=["POST"])
def predict():
    # Get the digit image from the request
    digit = request.files["digit"]
    digit = np.array([np.fromstring(digit.read(), np.uint8)])
    digit = digit.reshape((1, 28, 28, 1))

    # Make a prediction
    prediction = model.predict(digit)
    predicted_class = np.argmax(prediction)

    return jsonify({"prediction": str(predicted_class)})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)

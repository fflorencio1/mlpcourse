import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.utils import to_categorical

# Load the dataset
(x_train, y_train), _ = mnist.load_data()

# Normalize pixel values to the range [0, 1]
x_train = x_train / 255.0

# Transform labels into one-hot encoding
y_train_onehot = to_categorical(y_train, num_classes=10)

# Build the model
model = Sequential(
    [
        Flatten(input_shape=(28, 28)),
        Dense(128, activation="relu"),
        Dense(10, activation="softmax"),
    ]
)

model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])

model.fit(x_train, y_train_onehot, epochs=10, validation_split=0.15)

model.save("trained_model.h5")